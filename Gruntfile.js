const { option } = require('grunt');
let ruby = require('ruby');
let words = ruby.w('');

module.exports = function (grunt){ 
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
       useminPrepare: 'grunt-usemin'
    });
    grunt.initConfig({
        sass:{
            dist:{
                 files:[{
                        expand:true,
                        cwd:'scss',
                        src:['css/*.scss'],
                        dest:'css',
                        ext:'.css'
               }]
            }
        },
        watch:{
            files:['css/*.scss'],
            tasks:['css']
        }, 
        browserSync:{
            dev:{
                bsFiles:{
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                option:{
                    watchTask:true,
                    server:{
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },
        imagemin: {
            dynamic:{
                files:[{
                    expand:true,
                    cwd:'./',
                    src:'images/*.{png,git,jpg,jprg}',
                    dest: 'dist/'
                }]
            }
        },
        copy:{
            html:{
                files:[{
                    expand:true,
                    dot:true,
                    cwd:'./',
                    src:['*.html'],
                    dest: 'dist'
                }]
            },
            fonts:{
                files:[{
                    //for font-awesome
                    expand:true,
                    dot:true,
                    cwd:'node_modules/open-iconic/font',
                    src:['fonts/*.*'],
                    dest:'dist'
                }]
            }
        },
        clean:{
            build:{
                src:['dist/']
            }
        },
        cssmin:{
            dist:{}
        },
        uglify:{
            dist:{}
        },
        filerev:{
            options:{
                encoding:'utf8',
                algorithm:'md5',
                lenght:20
            },
            release:{
                //filerev:release hashes(md5) all assets (images, js and css)
                //in dist directory
                files:[{
                    src:[
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]
            }
        },
        concat:{
            options:{
                separator:';'
            },
            dist:{}
        },
        useminPrepare:{
            foo:{
                dest:'dist',
                src:['index.html', 'about.html', 'precios.html', 'contact.html', 'ollacomfee.html', 'olla-arrocera-aroma-houseware.html', 'Instant_pot.html']
            },
            option:{
                flow:{
                    steps:{
                        css:['cssmin']
                       // js:['uglify']
                    },
                    post:{
                        css:[{
                           name: 'cssmin',
                           createConfig:function(context, block){
                               var generated = context.options.generated;
                               generated.options = {
                                   keepSpecialComments:0,
                                   rebase:false
                               }
                           }             
                        }]
                    }     
                }
            }
            
        },
        usemin:{
            html:['dist/index.html', 'dist/about.html', 'dist/precios.html', 'dist/contac.html', 'dist/ollacomfee.html', 'dist/olla-arrocera-aroma-houseware.html', 'dist/Instant_pot.html' ],
            options:{
                assetsDir:['dist', 'dist/css', 'dist/js']
            }
        }
   });
    
     
    
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch'] );
    grunt.registerTask('img:compress',['imagemin']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
         ])
};




